var util = require('util');
var fs = require('fs');
var path = require('path');
var _ = require ('underscore')

var fileExtensions = [
  ".lua"
]

// these variable identifiers will be ignored, since they lead to too many false positives
var ignoreIdentifiers = [
  "val"
]

//set these in your config.json, not here
var config = {
  loglevel: "log", // 'error', 'warn', 'info', 'log'
  gamePath : 'C:\\Program Files\\Red 5 Studios\\Firefall'
}

var logger = require('./logger')
logger.debugLevel = config.loglevel;

try{
  var file = fs.openSync('./config.json', 'r')
  fs.close(file)
  config = require('./config.json')
}catch(e){
  var configFile = fs.writeFileSync('./config.json', JSON.stringify(config, null, 2))
  logger.warn('No config file found. Default file has been created')
}

config.gamePath = process.argv[2] || config.gamePath;

var traverseFileSystem = function (currentPath, list) {
  
  list = (typeof list === "undefined") ? [] : list;
  
  var files = fs.readdirSync(currentPath);
  for (var i in files) {
     var currentFile = currentPath + '/' + files[i];
     var stats = fs.statSync(currentFile);
     if (stats.isFile()) {
      if(fileExtensions.indexOf(path.extname(currentFile)) > -1){
         list.push(currentFile)
      }
    }
    else if (stats.isDirectory()) {
       list = traverseFileSystem(currentFile, list);
     }
    
   }
   return list
};
 
var buildSoundObject = function(collection, callback) {
  var inserted = 0;
  var sounds = {};
  var vars = {};
  for(var i = 0; i < collection.length; i++) {

    fs.readFile(collection[i], 'utf8', function(err, data) {
      if (err) {
        callback(err);
        return;
      }
      var sound_expr = /System.PlaySound\(\s*['"]{1}([^'"].+)['"]{1}\s*\)/g
      
      var var_expr = /System.PlaySound\(\s*([^'"].+)\s*\)/g
      

      while ((result = sound_expr.exec(data)) !== null) {
        sounds[result[1]] = result[1]
      }
      

      
      while ((result = var_expr.exec(data)) !== null) {
        
        try{
          
          var search = new RegExp( result[1]+'\\s*=\\s*\\"(.+)\\"', 'g' )
          
           while ((found = search.exec(data)) !== null) {
           
             if(ignoreIdentifiers.indexOf(result[1]) == -1){
                vars[found[1]] = found[1]
             }
          }
        }catch(e){
          //logger.log(result[1] + " is invalid")
        }

      }
      
      if (++inserted == collection.length) {
        callback(err, sounds, vars);
      }
      
    });
  }

}




var getGameVersion = function(cb){

  var exePath = config.gamePath + '\\system\\bin\\FirefallClient.exe';
  try{
    var file = fs.openSync(exePath, 'r')
    fs.close(file)
  }catch(e){
    logger.error(exePath + " could not be found. Check your config.json settings.")
    return;
  }

  var version = ""
  var spawn = require("child_process").spawn,child;
  child = spawn("powershell.exe",["(Get-Item \"" +config.gamePath + "\\system\\bin\\FirefallClient.exe\").VersionInfo.PRODUCTVERSION"]);
  child.stdout.on("data",function(data){
      version = data.toString().replace(/, /g, '.')
      version = version.replace(/[\r\n]/g, '')
  });
  child.stderr.on("data",function(data){
      logger.error(data.toString())
  });
  child.on("exit",function(){
      cb(version)
  });
  child.stdin.end(); //end input
}

getGameVersion(function(version){
  buildSoundObject(traverseFileSystem(config.gamePath + "\\system\\gui"), function(err, sounds, vars){
    if (err) {
      logger.error(err)
      return
    }
    
    var snd = Object.keys(sounds).map(function(x){return x})
    snd.sort()
    
    var vr = Object.keys(vars).map(function(x){return x})

    var diff = _.difference(vr, snd)
    diff.sort()
    
    logger.log("found " + snd.length + " sounds as strings")
    logger.log("found " + diff.length + " sounds as variables (unreliable)")

    
    if(!fs.existsSync("sounds")){
       fs.mkdirSync("sounds", 0766, function(err){
         if(err){ 
            logger.error(err);
         }
       });   
     }
    
    fs.writeFile("sounds\\"+version+"-sounds.txt",
      "Reliable:\r\n\r\n" + snd.join('\r\n') + "\r\n\r\n"+
      "Unreliable:\r\n\r\n" + diff.join('\r\n')
      , 'utf8', function (err) {
      if (err){
        logger.error(err)
        return
      }
      logger.info("successfully wrote " + (snd.length + diff.length) + " sounds to sounds\\" + version+"-sounds.txt")
    });
    

})
})

